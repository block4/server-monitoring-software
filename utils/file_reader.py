import time
from .watcher import DefaultAbstractWatcher


class Reader(DefaultAbstractWatcher):

    def __init__(self, q, file, interval):
        super(Reader, self).__init__()
        self.file = open(file, 'r')
        self.q = q
        self.interval = interval

    def watch(self):
        while self.run:
            new = self.file.readline()
            if new:
                self.on_new_line(self.q, new.strip())
            else:
                time.sleep(self.interval)

    def on_new_line(self, q, line):
        pass
