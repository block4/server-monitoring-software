import os
import socket

instance_name = None
ms_teams_url = None
database_host = None
database_port = None
database_name = None
database_user = None
database_password = None
email = False
email_smtp_server = None
email_smtp_port = None
email_smtp_user = None
email_smtp_passwd = None
email_sender = None
email_receivers = None
logfile = "/var/log/server-monitor.log"


def init():
    globals()["instance_name"] = socket.gethostname()
    if "LOGFILE" in os.environ.keys():
        globals()["logfile"] = os.environ['LOGFILE']
    globals()["ms_teams_url"] = os.environ['MSTEAMS_WEBHOOK_URL']
    globals()["database_host"] = os.environ["DB_HOST"]
    globals()["database_port"] = os.environ["DB_PORT"]
    globals()["database_name"] = os.environ["DB_DATABASE"]
    globals()["database_user"] = os.environ["DB_USER"]
    globals()["database_password"] = os.environ["DB_PASSWD"]
    if "EMAIL" in os.environ.keys():
        if os.environ["EMAIL"].lower() == "false":
            globals()["email"] = False
        elif os.environ["EMAIL"].lower() == "true":
            globals()["email"] = True
            globals()["email_smtp_server"] = os.environ["EMAIL_SMTP_SERVER"]
            globals()["email_smtp_port"] = os.environ["EMAIL_SMTP_PORT"]
            globals()["email_smtp_user"] = os.environ["EMAIL_SMTP_USER"]
            globals()["email_smtp_passwd"] = os.environ["EMAIL_SMTP_PASSWD"]
            globals()["email_sender"] = os.environ["EMAIL_SENDER"]
            globals()["email_receivers"] = os.environ["EMAIL_RECIVERS"]
