class DefaultAbstractWatcher:

    def __init__(self):
        self.run = True

    def watch(self):
        """ Contains a Loop for pulling data
        and puts the data in a Queue
        """
        pass

    def stop_watcher(self):
        self.run = False
