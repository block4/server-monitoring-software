from datetime import datetime

DATE_TIME_FORMAT = "%Y-%m-%d %H:%M:%S"


def current_date_time_string(format_string=DATE_TIME_FORMAT):
    return datetime.now().strftime(format_string)


def convert_string_to_date_time(time_string, format_string=DATE_TIME_FORMAT):
    return datetime.datetime.strptime(time_string, format_string)
