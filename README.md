# Server Monitoring Software

###### by Joint Venture of Titanic Inc. and Home-Office Spaßbad GmbH

## Configuration
**As env var or in /etc/server-monitoring-software.env**
* DB_HOST
* DB_PORT
* DB_DATABASE
* DB_USER
* DB_PASSWD
* MSTEAMS_WEBHOOK_URL
* EMAIL (true or false default: false)
* EMAIL_SMTP_SERVER 
* EMAIL_SMTP_USER 
* EMAIL_SMTP_PASSWD 
* EMAIL_SENDER 
* EMAIL_RECIVERS (string of emails with comma separation)

**Create Tables**
* Run this Python Script: [sql/CreateTables.py](sql/CreateTables.py)

## Dev requirements
* python 3.5 =<
* python-setuptools
* python-pip

## Build requirements
* (deb) `python3-all python3-pip python3-setuptools build-essential devscripts debhelper debmake dh-python`
* for building psycopg2 (deb) `postgresql-server-dev-all libpq-dev gcc`

## Runtime dependencies
* [_/postgres](https://hub.docker.com/_/postgres)
* [Teams](https://www.microsoft.com/de-de/microsoft-365/microsoft-teams/group-chat-software)
## Docker images:
* Pipeline images
    * [linalinn/python-postgres](https://hub.docker.com/r/linalinn/python-postgres)
    * [linalinn/python-deb-builder](https://hub.docker.com/r/linalinn/python-deb-builder)

## Commits
All Commits must start with a CAPSLOCK title with **3 to 12** characters followed by a **:** and at least one more character.
## Alert Messages
Every alert message has to use the following format:

**Alert Message**

\[Error type]

Message (eventually user information or exceeding values)

The text formatting uses MARKDOWN: 
- bold = `**Bold**`
- new line = `\n`
- link = `[link]`
- brackets without link = `\[ ]`

