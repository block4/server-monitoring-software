from unittest import TestCase
import time


class TestThread(TestCase):

    # TODO: automatic Stop Thread test
    #    def __init__(self,methodName=None):
    #        if methodName == None:
    #            super(TestCase, self).__init__()
    #        else:
    #            super(TestCase, self).__init__(methodName)
    #        self.Input = None

    def test_stop(self):
        if self.Input:
            self.Input.start()
            time.sleep(0.1)
            assert self.Input.is_Alive()
            self.Input.stop()
            time.sleep(0.2)
            assert not self.Input.is_Alive()
        else:
            TestCase.fail(self, "Test extends TestThread but dose not call setupThreadTest in __init__")
