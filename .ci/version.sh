#!/bin/bash
if [ -n "$CI_BUILD_TAG" ]
then
  sed -i "s/0.0.1/$CI_BUILD_TAG/g" $CI_PROJECT_DIR/setup.py
else
  sed -i "s/0.0.1/$CI_PIPELINE_ID/g" $CI_PROJECT_DIR/setup.py
fi