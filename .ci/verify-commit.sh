echo 'Verify commit...'

if [[ "$@" = Merge* ]]
then
    echo 'Commit is verified!'
    exit 0
fi

regex=[^a-z]{3,14}:.+
if [[ "$@" =~ $regex ]]
then 
    echo 'Commit is verified!'
else
    echo 'Verify commit failed! Commit Message matches not constraints.'
    echo "Regex for constraints: $regex"
    exit 42
fi
