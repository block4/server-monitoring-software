#!/usr/bin/env python3
from setuptools import setup
from setuptools import find_packages

setup(
    name='server-monitoring-software',
    version='0.0.1',
    scripts=['main.py'],
    install_requires=['psutil==5.7.0', 'psycopg2', 'pytest', 'requests'],
    packages=find_packages(),
    url='https://gitlab.com/block4/server-monitoring-software',
    license='Mozilla Public License Version 2.0',
    author='block4 GitLab Group',
    author_email='',
    description='Server Monitoring Software'
)
