from utils.time_calculation import current_date_time_string


class LogWriter:

    def __init__(self, file):
        self.file = file

    def write(self, module, log_msg):
        self.write(current_date_time_string(), module, log_msg)

    def write(self, date, module, log_msg):
        self.file.write("[ {} | {} ]: {}\n".format(date, module, str(log_msg).strip()))
