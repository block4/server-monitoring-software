from threading import Thread
from utils.Input import InputInterface
from utils.file_reader import Reader


class AuthLog(Reader, InputInterface):

    def __init__(self, q, file, interval):
        super(AuthLog, self).__init__(q, file, interval)
        self.thread = Thread(target=self.watch)
        # TODO: Check time of startup with line and ignore if it is older

    def on_new_line(self, q, line):
        if " sshd[" in line:
            if "]: Invalid user" in line:
                q.put(line)
            elif "]: Failed password for" in line:
                q.put(line)
            elif "]: Accepted password for" in line:
                q.put(line)
            elif "]: Disconnected from user" in line:
                q.put(line)
        elif " sudo: pam_unix(sudo:auth):" in line:
            q.put(line)

    def start(self):
        self.thread.start()

    def is_Alive(self):
        return self.thread.is_alive()

    def stop(self):
        self.stop_watcher()
