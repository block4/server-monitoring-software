from unittest import TestCase

from logfiles.log_writer import LogWriter


class MockFile:
    line = None

    def write(self, line):
        self.line = line


class TestLogWriter(TestCase):
    date = "Fr 12. Jun 17:11:01 CEST 2020"
    line = "Jun  9 08:02:58 test sshd[000]: pam_unix(sshd:auth): check pass; user test"
    module = "unittest"

    def test_write(self):
        expect = "[ " + self.date + " | " + self.module + " ]: " + self.line + "\n"
        test_obj = LogWriter(MockFile())
        test_obj.write(self.date, self.module, self.line)
        self.assertEqual(expect, test_obj.file.line)
