import psycopg2


class CreateTables:

    def __init__(self, host, port, database, user, password):
        self.conn = psycopg2.connect(host=host, port=port, database=database, user=user, password=password)
        self.cursor = self.conn.cursor()

    def disconnect(self):
        if self.cursor is not None:
            self.cursor.close()
        if self.conn is not None:
            self.conn.close()

    def create_tables(self):
        print("Create Table cpuload...")
        self.cursor.execute("DROP TABLE IF EXISTS public.cpuload")
        self.cursor.execute("""CREATE TABLE public.cpuload (
            id SERIAL UNIQUE NOT NULL,
            load FLOAT NOT NULL,
            datetime TEXT NOT NULL,
            PRIMARY KEY (id)
        )""")

        print("Create Table procnum...")
        self.cursor.execute("DROP TABLE IF EXISTS public.procnum")
        self.cursor.execute("""CREATE TABLE public.procnum (
            id SERIAL UNIQUE NOT NULL,
            number FLOAT NOT NULL,
            datetime TEXT NOT NULL,
            PRIMARY KEY (id)
        )""")

        print("Create Table ramusage...")
        self.cursor.execute("DROP TABLE IF EXISTS public.ramusage")
        self.cursor.execute("""CREATE TABLE public.ramusage (
            id SERIAL UNIQUE NOT NULL,
            usage FLOAT NOT NULL,
            datetime TEXT NOT NULL,
            PRIMARY KEY (id)
        )""")

        print("Changes will be saved!")
        self.conn.commit()


print("Create Tables started!")
print("WARNING! All tables are newly created. Existing tables are deleted!")
print("")

host = str(input("Database Host: "))
port = str(input("Database Port: "))
database = str(input("Database Name: "))
user = str(input("Database User: "))
password = str(input("Database Password: "))

initialize = CreateTables(host, port, database, user, password)
initialize.create_tables()
initialize.disconnect()
password = None

print("Create Tables finished!")
