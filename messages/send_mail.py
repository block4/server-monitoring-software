import smtplib


class SendMail:
    message_template = """From: {1}
To: {2}
Subject: {3}

{4}

Send by server-monitoring-software {0}
"""

    def __init__(self, host, port, user, passwd, sender, receivers, instance_name, TLS=True):
        if TLS:
            self.smtp = smtplib.SMTP_SSL(host, port)
        else:
            self.smtp = smtplib.SMTP(host, port)
        self.smtp.login(user, passwd)
        self.sender = sender
        self.receivers = receivers
        self.instance_name = instance_name

    def send(self, sub, message):
        receivers = self.format_receivers()
        msg = self.message_template.format(self.instance_name, self.sender, receivers, sub, message)
        self.smtp.sendmail(self.sender, self.receivers_to_list(), msg)

    def format_receivers(self):
        if isinstance(self.receivers, list):
            receivers = ""
            for receiver in self.receivers:
                receivers += receiver + ", "
            return receivers[:-2]
        else:
            return self.receivers

    def receivers_to_list(self):
        if "," in self.receivers:
            result = self.receivers.replace(" ", "").split(",")
            return result
        return self.receivers
