import requests

from utils import config


class SendMessage:

    def __init__(self):
        self.payload = {}

    def send_my_message(self, message_type=0, error_msg="", message="", url=""):
        if url == "":
            url = config.ms_teams_url
        if message != "":
            if message_type == 0:
                msg_t = "**Alert Message**   \n"
            elif message_type == 1:
                msg_t = "**Information**   \n"

            self.payload["text"] = msg_t + error_msg + "   \n" + message
            headers = {"Content-Type": "application/json"}
            r = requests.post(
                url,
                json=self.payload,
                headers=headers,
                timeout=60
            )
            if r.status_code == requests.codes.ok:
                return True
            else:
                return False


"""
error = "[Error Code]"
msg = "Send it dear God just FUCKING SEND IT! AHHHHH, I'm gonna kill myself. I'll kill myself and it's your fault!"
fucking_send_it = SendMessage
fucking_send_it.send_message(fucking_send_it, error, msg)
"""
