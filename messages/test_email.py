import unittest
from unittest import mock

from messages.send_mail import SendMail

instance_name = "unittest_instance_name"
receivers = ["recv1@unitest", "recv2@unittest"]
sender = "sender@unitest"
port = 465
host = "none"
sub = "unittest"
msg = """unittest
are
grate"""


class SmtpMock:
    msg = ""

    def __init__(self, *args):
        print(args)

    def sendmail(self, ignore1, ignore2, message):
        self.msg = message
        _ = ignore1
        _ = ignore2

    def login(self, ignore1, ignore2):
        _ = ignore1
        _ = ignore2

    def get_msg(self):
        return self.msg


class MyTestCase(unittest.TestCase):

    def test_receivers_to_list(self):
        with mock.patch("smtplib.SMTP_SSL", SmtpMock):
            expect = ["recv1@unitest", "recv2@unittest"]
            test_obj = SendMail(host, port, "user", "passwd", sender, "recv1@unitest, recv2@unittest", instance_name)
            self.assertEqual(expect, test_obj.receivers_to_list())

    def test_receivers_to_list_singel(self):
        with mock.patch("smtplib.SMTP_SSL", SmtpMock):
            expect = "recv1@unitest"
            test_obj = SendMail(host, port, "user", "passwd", sender, "recv1@unitest", instance_name)
            self.assertEqual(expect, test_obj.receivers_to_list())

    def test_receivers_to_list_with_name(self):
        with mock.patch("smtplib.SMTP_SSL", SmtpMock):
            expect = ["\"recv1@unitest\"<recv1@unitest>", "\"recv2@unittest\"<recv2@unittest>"]
            test_obj = SendMail(host, port, "user", "passwd", sender,
                                "\"recv1@unitest\" <recv1@unitest>, \"recv2@unittest\" <recv2@unittest>", instance_name)
            self.assertEqual(expect, test_obj.receivers_to_list())

    def test_format_receivers(self):
        with mock.patch("smtplib.SMTP_SSL", SmtpMock):
            expect = "recv1@unitest, recv2@unittest"
            test_obj = SendMail(host, port, "user", "passwd", sender, "recv1@unitest, recv2@unittest", instance_name)
            self.assertEqual(expect, test_obj.format_receivers())

    def test_format_receivers2(self):
        with mock.patch("smtplib.SMTP_SSL", SmtpMock):
            expect = "recv1@unitest, recv2@unittest"
            test_obj = SendMail(host, port, "user", "passwd", sender, receivers, instance_name)
            self.assertEqual(expect, test_obj.format_receivers())

    def test_send(self):
        with mock.patch("smtplib.SMTP_SSL", SmtpMock):
            test_obj = SendMail(host, port, "user", "passwd", sender, receivers, instance_name)
            test_obj.send(sub, msg)

        expect = """From: sender@unitest
To: """ + receivers[0] + """, """ + receivers[1] + """
Subject: """ + sub + """

""" + msg + """

Send by server-monitoring-software """ + instance_name + """
"""

        self.assertEqual(expect, test_obj.smtp.get_msg())


if __name__ == '__main__':
    unittest.main()
