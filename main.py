#!/usr/bin/env python3
import traceback

from logfiles.log_writer import LogWriter
from messages.send_mail import SendMail
from utils import config
from database_connection.DatabaseConnection import DatabaseConnection
from multiprocessing import Queue
from cpu_usage.ReadSystemUtilization import readCPUThread, readProcessNumThread, readRamThread, get_highest_ram_usage
import time
from messages.SendMessage import SendMessage
from serverlogs_database.ServerlogsDatabase import ServerlogsDatabase
from utils.time_calculation import current_date_time_string

watchers = []
queues = []

db = DatabaseConnection()

teams = SendMessage()

log_to_file = None
email = None


def main():
    db.connect(host=config.database_host,
               port=config.database_port,
               database=config.database_name,
               user=config.database_user,
               password=config.database_password)

    logging = ServerlogsDatabase(database=db)

    q_cpu = Queue()
    q_ram = Queue()
    q_processes = Queue()
    queues.append(q_cpu)
    queues.append(q_ram)
    queues.append(q_processes)

    cpu = readCPUThread(q_cpu, "CPU-Thread")
    ram = readRamThread(q_ram, "RAM-Thread")
    processes = readProcessNumThread(q_processes, "Process_Num-Thread")
    watchers.append(cpu)
    watchers.append(ram)
    watchers.append(processes)

    for w in watchers:
        w.start()

    while cpu.is_Alive() and ram.is_Alive() and processes.is_Alive():
        if not q_cpu.empty():
            cpu_load = q_cpu.get()
            if cpu_load > 60.0:
                send_msg(sub="CPU LOAD", message="Load: " + str(cpu_load) + "%")
            if cpu_load > 50.0:
                log_to_file.write(current_date_time_string(), "CPU LOAD", str(cpu_load))
            logging.add_system_utilization(cpu_load, cpu.thread_name)

        if not q_ram.empty():
            vram = q_ram.get()
            if vram > 80.0:
                vram_string = get_highest_ram_usage()
                send_msg(sub="RAM USAGE", message="Usage: " + str(vram) + "%" + vram_string)
            if vram > 70.0:
                log_to_file.write(current_date_time_string(), "RAM USAGE", str(cpu_load))
            logging.add_system_utilization(vram, ram.thread_name)
        if not q_processes.empty():
            process_number = q_processes.get()
            if process_number > 400:
                send_msg(sub="PROCESSES", message="Number of processes: " + str(process_number))
            if process_number > 150:
                log_to_file.write(current_date_time_string(), "processes", str(process_number))
            logging.add_system_utilization(process_number, processes.thread_name)
        time.sleep(0.1)
    pass


def send_msg(message, sub="", message_type=0):
    # TODO: email rate limit
    # if email:
    #    email.send(sub, message)
    teams.send_my_message(message_type=message_type, error_msg="\[" + sub + " (" + config.instance_name + ")]",
                          message=message)


def init():
    global log_to_file, email
    config.init()
    if config.email:
        email = SendMail(config.email_smtp_server, config.email_smtp_port, config.email_smtp_user,
                         config.email_smtp_passwd, config.email_sender, config.email_receivers, config.instance_name)
    log_to_file = LogWriter(open(config.logfile, "a"))


if __name__ == "__main__":
    try:
        init()
        send_msg(message_type=1, sub="STATUS", message=" Starting...")
        if email:
            email.send("[" + config.instance_name + "]: Starting...", "[" + config.instance_name + "]: Starting...")
        main()
    except KeyboardInterrupt:
        print("Stopping Watchers")
        for watcher in watchers:
            watcher.stop()
        db.disconnect()
        send_msg(message_type=1, sub="STATUS", message=" ...Stopped")
        if email:
            email.send("[" + config.instance_name + "]:...Stopped", "[" + config.instance_name + "]:...Stopped")
        log_to_file.file.flush()
    except Exception:
        print(str(traceback.format_exc()))
        try:
            teams.send_my_message(error_msg="\[FATAL ERROR (" + config.instance_name + ")]",
                                  message=str(traceback.format_exc()))
        except Exception:
            print(str(traceback.format_exc()))
        try:
            log_to_file.write(current_date_time_string(), "FATAL ERROR", str(traceback.format_exc()).strip())
            log_to_file.file.flush()
        except Exception:
            print(str(traceback.format_exc()))
