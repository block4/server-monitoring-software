# CHANGELOG

## Release 0.0.3
* Output Module
   * Email
   * Logfile
* Tools
   * setup.sh

## Release 0.0.2
* Pipeline
   * Automatic version number
* systemd
   * KillSignal=SIGINT
* utils
   * config.py
* Input Module
   * RAM usage
   * number of processes
* Output Module
   * MS Teams
      * highest RAM usage list
      * amount of processes
* Tools
   * CreateTables.py

## Release 0.0.1
* Pipeline
   * flake8 code review
   * Unit testing
   * Building Python application
   * Packaging app to Debian Package
   * Deployment to Server
* Input Module
   * CPU Load
* Output Module
   * MS Teams
   * PostgreSQL Database
   