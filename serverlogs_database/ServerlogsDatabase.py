from datetime import datetime


class ServerlogsDatabase:

    def __init__(self, database):
        self.database = database

    def add_system_utilization(self, param, t_name):
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        value = str(param)
        if t_name == "CPU-Thread":
            self.database.\
                execute_update("insert into cpuload (load, datetime) VALUES ('" + value + "', '" + timestamp + "')")
        elif t_name == "RAM-Thread":
            self.database.\
                execute_update("insert into ramusage (usage, datetime) VALUES ('" + value + "', '" + timestamp + "')")
        elif t_name == "Process_Num-Thread":
            self.database.\
                execute_update("insert into procnum (number, datetime) VALUES ('" + value + "', '" + timestamp + "')")

    def remove_value(self, value_id, t_name):
        if t_name == "CPU-Thread":
            self.database.execute_update("delete from cpuload where id = '" + str(value_id) + "'")
        elif t_name == "RAM-Thread":
            self.database.execute_update("delete from ramusage where id = '" + str(value_id) + "'")
        elif t_name == "Process_Num-Thread":
            self.database.execute_update("delete from procnum where id = '" + str(value_id) + "'")
        return True

    def get_values(self, t_name):
        if t_name == "CPU-Thread":
            return self.database.execute_query("select * from cpuload")
        elif t_name == "RAM-Thread":
            return self.database.execute_query("select * from ramusage")
        elif t_name == "Process_Num-Thread":
            return self.database.execute_query("select * from procnum")
