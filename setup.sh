#!/usr/bin/env bash

echo "setup env (make sure venv is intalled and pip) y/n"
read venv_init
if [ $venv_init == "y" ] || [ $venv_init == "Y" ]
then
  echo "this may fail if so try install psycopg2-binary  or the requirements for building psycopg2"
  python3 -m venv env
  source env/bin/activate
  pip install -r requirements.txt
  echo "this may fail if so try install psycopg2-binary  or the requirements for building psycopg2"
fi
echo "Enter webook url"
read webook
echo "webook $webook"
echo "set database host"
read db_host
echo "database host $db_host"
echo "set database port"
read db_port
echo "database port $db_port"
echo "set database name (if you want an this script to automaticly setup the db with docker use postgres as database)"
read db_name
echo "database name $db_name"
echo "set database user (if you want an this script to automaticly setup the db with docker use postgres as user)"
read db_user
echo "database user $db_user"
echo "set database passwd"
read db_passwd
echo "database passwd $db_passwd"

echo "set logfile default /var/log/server-monitor.log"
read logfile

echo "email set true or false"
read email

if [ $email == "true" ]
then
  echo "set email smtp server"
  read email_smtp_server
  echo "email smtp server $email_smtp_server"
  echo "set email smtp port"
  read email_smtp_port
  echo "email smtp port $email_smtp_port"
  echo "set email smtp user"
  read email_smtp_user
  echo "email smtp user $email_smtp_user"
  echo "set email smtp passwd"
  read email_smtp_passwd
  echo "email smtp passwd $email_smtp_passwd"
  echo "set email_sender"
  read email_sender
  echo "email sender $email_sender"
  echo "set email receivers"
  read email_receivers
  echo "email receivers $email_receivers"
  email="EMAIL=ture;EMAIL_SMTP_SERVER=$email_smtp_server;EMAIL_SMTP_PORT=$email_smtp_port;EMAIL_SMTP_USER=$email_smtp_user;EMAIL_SMTP_PASSWD=$email_smtp_passwd;EMAIL_SENDER=$email_sender;EMAIL_RECIVERS=$email_receivers"
else
  email="EMAIL=false;"
fi
echo "LOGFILE=$logfile;MSTEAMS_WEBHOOK_URL=$webook;DB_HOST=$db_host;DB_PORT=$db_port;DB_DATABASE=$db_name;DB_USER=$db_user;DB_PASSWD=$db_passwd;$email"
echo "set up db with docker? y/n"
read db_setup
if [ $db_setup == "y" ] || [ $db_setup == "Y" ]
then
  echo "set container name"
  read container_name
  echo "docker run --name $container_name -p $db_port:5432 -e POSTGRES_PASSWORD=$db_passwd -d postgres"
  docker run --name $container_name -p $db_port:5432 -e POSTGRES_PASSWORD=$db_passwd -d postgres
fi

echo "generate init or update db command? y/n"
read db_init
if [ $db_init == "y" ] || [ $db_init == "Y" ]
then
  echo "echo \"$db_host\n$db_port\n$db_name\n$db_user\n$db_passwd\" | python3 sql/CreateTables.py"
fi
