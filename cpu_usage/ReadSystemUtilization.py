import time
from threading import Thread
import psutil
from utils.Input import InputInterface
from utils.watcher import DefaultAbstractWatcher


class systemUtilizationThread(DefaultAbstractWatcher, InputInterface):

    def __init__(self, q, t_name):
        super(DefaultAbstractWatcher, self).__init__()
        self.q = q
        self.run = True
        self.interval = 5
        self.thread_name = t_name
        self.thread = Thread(target=self.watch, name=self.thread_name)

    def watch(self):
        while self.run:
            self.execute()

    def execute(self):
        self.stop()

    def start(self):
        self.thread.start()

    def is_Alive(self):
        return self.thread.is_alive()

    def stop(self):
        """
        AttributeError: 'cpuThread' object has no attribute 'run'
        even DefaultAbstractWatcher has it
        """
        self.run = False


class readCPUThread(systemUtilizationThread):
    def __init__(self, q, t_name):
        systemUtilizationThread.__init__(self, q, t_name)

    def execute(self):
        cpu_percentage = psutil.cpu_percent()
        if (cpu_percentage > 0.0):
            self.q.put(cpu_percentage)
        time.sleep(self.interval)


class readRamThread(systemUtilizationThread):
    def __init__(self, q, t_name):
        systemUtilizationThread.__init__(self, q, t_name)

    def execute(self):
        vram = psutil.virtual_memory()
        vram_used = vram.used / vram.total * 100
        vram_used = round(vram_used, 2)
        self.q.put(vram_used)
        time.sleep(self.interval)


class readProcessNumThread(systemUtilizationThread):
    def __init__(self, q, t_name):
        systemUtilizationThread.__init__(self, q, t_name)

    def execute(self):
        process_number = 0
        for pr in psutil.process_iter():
            process_number += 1
        self.q.put(process_number)
        time.sleep(self.interval)


def get_highest_ram_usage():

    process_list = []
    p_string = "   \n"
    vram = psutil.virtual_memory()
    for pr in psutil.process_iter():
        try:
            proc_info = pr.as_dict(attrs=["pid", "name"])
            proc_info["p_vram"] = round(pr.memory_info().vms / vram.total * 100, 2)
            process_list.append(proc_info)
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    process_list = sorted(process_list, key=lambda proc_list: proc_list["p_vram"], reverse=True)[:5]
    for i in range(0, len(process_list)):
        p_string += "Process: " + process_list[i]["name"] + " RAM-usage: " + str(process_list[i]["p_vram"])
        p_string += "%   \n"
    return p_string
