from unittest import TestCase
from unittest import mock
import time
from queue import Queue

from . import ReadSystemUtilization


class TestSystemUtilizationThread(TestCase):
    Input = ReadSystemUtilization

    def test_watch(self):
        queue = Queue()
        testObject_cpu = self.Input.readCPUThread(queue, "CPU-Thread")
        with mock.patch("psutil.cpu_percent", return_value=50):
            testObject_cpu.execute()
            assert queue.get() == 50

    def test_stop(self):
        queue = Queue()
        testObject = self.Input.readCPUThread(queue, "CPU-Thread")
        testObject.start()
        time.sleep(0.1)
        assert testObject.is_Alive()
        testObject.stop()
        time.sleep(testObject.interval + 0.5)
        assert not testObject.is_Alive()
