import psycopg2


class DatabaseConnection:

    def __init__(self):
        self.cursor = None
        self.conn = None

    def connect(self, host, port, database, user, password):
        self.conn = psycopg2.connect(host=host, port=port, database=database, user=user, password=password)
        self.cursor = self.conn.cursor()
        return True

    def disconnect(self):
        if self.cursor is not None:
            self.cursor.close()
        if self.conn is not None:
            self.conn.close()

    def execute_query(self, query_statement):
        self.cursor.execute(query_statement)
        result = self.cursor.fetchall()
        return result

    def execute_update(self, update_statement):
        self.cursor.execute(update_statement)
        self.conn.commit()
        return True
