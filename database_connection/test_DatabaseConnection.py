import unittest
from unittest import mock

from database_connection.DatabaseConnection import DatabaseConnection


class TestDatabaseConnection(unittest.TestCase):

    def test_execute_query(self):
        db = DatabaseConnection()
        # Here is already tested that the connection works
        with mock.patch.object(DatabaseConnection, 'connect', return_value=True):
            connection = db.connect("it9l.de", "diedatenbank", "postgres", "thecoolestpassword")
            self.assertEqual(True, connection)
        with mock.patch.object(DatabaseConnection, 'execute_query', return_value=[(1, 'beispieltext')]):
            query = db.execute_query("select * from log")
            self.assertEqual([(1, 'beispieltext')], query)

    def test_execute_update(self):
        db = DatabaseConnection()
        with mock.patch.object(DatabaseConnection, 'connect', return_value=True):
            connection = db.connect("it9l.de", "diedatenbank", "postgres", "thecoolestpassword")
            self.assertEqual(True, connection)
        with mock.patch.object(DatabaseConnection, 'execute_update', return_value=True):
            update = db.execute_update("UPDATE log SET message = 'New Message' WHERE id = 1")
            self.assertEqual(True, update)


if __name__ == '__main__':
    unittest.main(verbosity=2)
